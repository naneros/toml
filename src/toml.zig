const std = @import("std");
const debug = std.debug;
const assert = debug.assert;
const testing = std.testing;
const parseInt = std.fmt.parseInt;
const parseFloat = std.fmt.parseFloat;

pub const Position = struct {
    row: usize = 0,
    col: usize = 0,
};

pub const String = struct {
    multiline: bool = false,
    literal: bool = true,
    escapes: bool = false,
    slice: []const u8,
};

pub const Integer = struct {
    radix: u8,
    slice: []const u8,
};

pub const Token = union(enum) {
    None,
    Identifier: String,
    TableBegin,
    TableEnd,
    TableArrayBegin,
    TableArrayEnd,
    ArrayBegin,
    ArrayEnd,
    InlineTableBegin,
    InlineTableEnd,
    Dot,
    Comma,
    Equals,
    EndLine,

    Comment: []const u8,
    Integer: Integer,
    Float: []const u8,
    Boolean: bool,
    String: String,
    DateTime: []const u8,
    LocalDateTime: []const u8,
    LocalDate: []const u8,
    LocalTime: []const u8,
};

const Container = enum(u3) { 
    None,
    Table,
    TableArray,
    Array, 
    InlineTable, 
};

const max_nestings = 32;

const Nesting = struct {
    stack: [max_nestings]Container = undefined,
    len: usize = 0,
    state: Container = .None,
    
    pub fn open(self: *Nesting, c: Container) Error!void {
        if (self.len == max_nestings) return error.ManyNested;
        switch (c) {
            .Table, .TableArray => {
                if (self.len > 0) return error.UnexpectedOpen;
            },
            else => {}
        }
        self.stack[self.len] = c;
        self.len += 1;
        self.state = c;
    }
    
    pub fn close(self: *Nesting, c: Container) Error!void {
        if (self.len == 0) return error.UnexpectedClose;
        if (c != self.state) return error.UnexpectedClose;
        self.len -= 1;
        if (self.len == 0) self.state = .None 
            else self.state = self.stack[self.len - 1];
    }
};

pub const Error = error{
    OutOfMemory,
    ManyNested,
    UnexpectedOpen,
    UnexpectedClose,
    InvalidCharacter,
    InvalidValue,
    InvalidStringClose,
    Overflow,
    UnexpectedToken,
    UnexpectedEnd,
    UnexpectedEndLine,
    UnclosedTable,
    UnclosedTableArray,
    UnclosedArray,
    UnclosedInlineTable,
    UnclosedString,
    
    KeyAlreadyExist,
};

fn isNumber(c :u8) bool {
    switch (c) {
        '0'...'9' => return true,
        else => return false,
    }
}

fn isValidUtf(s: []const u8) bool {
    if (s.len != 4 and s.len != 8) return false;
    for (s) |c| {
        switch (c) {
            '0'...'9', 'a'...'f', 'A'...'F' => {},
            else => return false,
        }
    }
    
    const cp = std.fmt.parseInt(u32, s, 16) catch return false;
    if ((cp > 0xd7ff and cp < 0xe000) or (cp > 0x10ffff)) return false;
    return true;
}

pub const Tokenizer = struct {
    i: usize,
    input: []const u8,
    state: State,
    prev_state: State,
    cs: Nesting,
    start: usize,
    multiline: bool,
    string_end: bool,
    dtm_offset: usize,
    id_string: bool,
    escapes: bool,
    need_id: bool,
    pos: ?*Position,
    
    pub fn init(input: []const u8, pos: ?*Position) Tokenizer {
        var self: Tokenizer = undefined;
        self.pos = pos;
        self.input = input;
        self.i = 0;
        self.state = .NewLine;
        self.prev_state = .NewLine;
        self.cs = Nesting{};
        self.start = 0;
        self.multiline = false;
        self.string_end = false;
        self.dtm_offset = 0;
        self.id_string = false;
        self.escapes = false;
        self.need_id = false;
        if (self.pos) |p| { p.row = 1; p.col = 1; }
        return self;
    }
    
    const State = enum(u8) {
        NewLine,
        Comment,
        LineDone,
        TableOrTableArray,
        ClosingTableArray,
        Identifier,
        IdentifierBegin,
        IdentifierEnd,
        ValueBegin,
        ValueEnd,
        
        StringLiteral,
        StringBasic,
        StringEscape,
        ToEndLine,
        
        Zero,
        Sign,
        SignZero,
        NumberOrDateTime,
        Number,
        FloatFract,
        FloatFractional,
        FloatExponent,
        FloatExponentSign,
        Float,
        Binary,
        Octal,
        Hexa,
        
        DateTime,
        LocalTime,
        DateTimeMiliseconds,
    };
    
    pub fn next(self: *Tokenizer) Error!?Token {
        var token: ?Token = null;
        while (self.i <= self.input.len) {
            if (self.i == self.input.len) {
                switch (self.cs.state) {
                    .None => {},
                    .Table => return error.UnclosedTable,
                    .TableArray => return error.UnclosedTableArray,
                    .Array => return error.UnclosedArray,
                    .InlineTable => return error.UnclosedInlineTable,
                }
                switch (self.state) {
                    .StringBasic, .StringLiteral => return error.UnclosedString,
                    else => {},
                }
                try self.transition('\n', &token); // last token
            } else {
                try self.transition(self.input[self.i], &token);
                if (self.pos) |p| {
                    if (self.input[self.i] == '\n') {
                        p.row += 1;
                        p.col = 1;
                    } else p.col += 1;
                    
                }
            }
            self.i += 1;
            
            if (token) |t| return t;
        }
        return null;
    }
    
    fn curSlice(self: *Tokenizer, s: []const u8) bool {
        if (s.len > self.input.len - self.i) return false;
        for (s, 0..) |c, i| {
            if (c != self.input[self.i + i]) return false;
        }
        return true;
    }

    fn transition(s: *Tokenizer, c: u8, token: *?Token) Error!void {
        switch (s.state) {
            .Comment => switch (c) {
                '\n' => {
                    if (s.cs.state == .Array) s.state = s.prev_state
                    else {
                        token.* = Token.EndLine;
                        s.state = .NewLine;
                    }
                },
                13 => if (!s.curSlice("\r\n")) return error.InvalidCharacter,
                0, 8, 16, 31, 127...255 => return error.InvalidCharacter,
                else => {},
            },
            .LineDone => switch (c) {
                ' ' => {},
                '\n' => {
                    token.* = Token.EndLine;
                    s.state = .NewLine;
                },
                13 => if (!s.curSlice("\r\n")) return error.InvalidCharacter,
                '#' => s.state = .Comment,
                else => {
                    return error.InvalidCharacter;
                },
            },
            .NewLine => switch (c) {
                ' ', '\t', '\n' => {},
                13 => if (!s.curSlice("\r\n")) return error.InvalidCharacter,
                'a'...'z', 'A'...'Z', '0'...'9', '-', '_' => {
                    s.start = s.i;
                    s.state = .Identifier;
                },
                '\'' => {
                    s.start = s.i + 1;
                    s.multiline = false;
                    s.id_string = true;
                    s.state = .StringLiteral;
                },
                '"' => {
                    s.start = s.i + 1;
                    s.multiline = false;
                    s.id_string = true;
                    s.escapes = false;
                    s.state = .StringBasic;
                },
                '[' => {
                    s.state = .TableOrTableArray;
                },
                '#' => {
                    s.state = .Comment;
                },
                else => return error.InvalidCharacter,
            },
            
            .IdentifierBegin => switch (c) {
                ' ', '\t' => {},
                'a'...'z', 'A'...'Z', '0'...'9', '-', '_' => {
                    s.need_id = false;
                    s.start = s.i;
                    s.state = .Identifier;
                },
                '\'' => {
                    s.need_id = false;
                    s.start = s.i + 1;
                    s.multiline = false;
                    s.id_string = true;
                    s.state = .StringLiteral;
                },
                '"' => {
                    s.need_id = false;
                    s.start = s.i + 1;
                    s.multiline = false;
                    s.id_string = true;
                    s.escapes = false;
                    s.state = .StringBasic;
                },
                '}' => {
                    if (s.cs.state != .InlineTable or s.need_id) return error.InvalidCharacter;
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
                else => {
                    // std.log.warn("{}", .{c});
                    return error.InvalidCharacter;
                }
            },
            .Identifier => switch (c) {
                'a'...'z', 'A'...'Z', '0'...'9', '-', '_' => {},
                else => {
                    token.* = .{ .Identifier = .{ .slice = s.input[s.start..s.i] } };
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .IdentifierEnd;
                },
            },
            .IdentifierEnd => switch (c) {
                ' ', '\t' => {},
                '.' => {
                    token.* = .Dot;
                    s.state = .IdentifierBegin;
                },
                '=' => {
                    token.* = .Equals;
                    s.state = .ValueBegin;
                },
                ']' => switch (s.cs.state) {
                    .Table => {
                        try s.cs.close(.Table);
                        token.* = .TableEnd;
                        s.state = .LineDone;
                    },
                    .TableArray => {
                        s.state = .ClosingTableArray;
                    },
                    else => return error.InvalidCharacter,
                },
                else => {
                    // std.log.warn("{c}", .{c});
                    return error.InvalidCharacter;
                }
            },
            
            .TableOrTableArray => switch (c) {
                ' ', '\t' => {
                    try s.cs.open(.Table);
                    token.* = .TableBegin;
                    s.state = .IdentifierBegin;
                },
                'a'...'z', 'A'...'Z', '0'...'9', '-', '_' => {
                    try s.cs.open(.Table);
                    token.* = .TableBegin;
                    s.start = s.i;
                    s.state = .Identifier;
                },
                '\'' => {
                    try s.cs.open(.Table);
                    token.* = .TableBegin;
                    
                    s.start = s.i + 1;
                    s.multiline = false;
                    s.id_string = true;
                    s.state = .StringLiteral;
                },
                '"' => {
                    try s.cs.open(.Table);
                    token.* = .TableBegin;

                    s.start = s.i + 1;
                    s.multiline = false;
                    s.id_string = true;
                    s.escapes = false;
                    s.state = .StringBasic;
                },
                '[' => {
                    try s.cs.open(.TableArray);
                    token.* = .TableArrayBegin;
                    s.state = .IdentifierBegin;
                },
                else => return error.InvalidCharacter,
            },
            .ClosingTableArray => switch (c) {
                ']' => {
                    try s.cs.close(.TableArray);
                    token.* = .TableArrayEnd;
                    s.state = .LineDone;
                },
                else => return error.InvalidCharacter,
            },
            .ValueBegin => switch (c) {
                '#' => {
                    if (s.cs.state == .Array) {
                        s.prev_state = .ValueBegin;
                        s.state = .Comment;
                    } else return error.InvalidCharacter;
                },
                ' ', '\t' => {},
                '+', '-' => {
                    s.start = s.i;
                    s.state = .Sign;
                },
                '0' => {
                    s.start = s.i;
                    s.state = .Zero;
                },
                '1'...'9' => {
                    s.start = s.i;
                    s.state = .NumberOrDateTime;
                },
                'n' => switch (s.curSlice("nan")) {
                    true => {
                        token.* = Token { .Float = s.input[s.i..s.i + 3] };
                        s.i += 2;
                        s.state = .ValueEnd;
                    },
                    false => return error.InvalidValue,
                },
                'i' => switch (s.curSlice("inf")) {
                    true => {
                        token.* = Token{ .Float = s.input[s.i..s.i + 3] };
                        s.i += 2;
                        s.state = .ValueEnd;
                    },
                    false => return error.InvalidValue,
                },
                
                '\'' => {
                    s.multiline = s.curSlice("'''");
                    if (s.multiline) s.i += 2;
                    s.start = s.i + 1;
                    s.id_string = false;
                    s.state = .StringLiteral;
                },
                '"' => {
                    s.multiline = s.curSlice("\"\"\"");
                    if (s.multiline) s.i += 2;
                    s.start = s.i + 1;
                    s.id_string = false;
                    s.escapes = false;
                    s.state = .StringBasic;
                },
                't' => switch (s.curSlice("true")) {
                    true => {
                        token.* = Token { .Boolean = true};
                        s.i += 3;
                        s.state = .ValueEnd;
                    },
                    false => return error.InvalidValue,
                },
                'f' => switch (s.curSlice("false")) {
                    true => {
                        token.* = Token { .Boolean = false};
                        s.i += 4;
                        s.state = .ValueEnd;
                    },
                    false => return error.InvalidValue,
                },
                '[' => {
                    try s.cs.open(.Array);
                    token.* = Token.ArrayBegin;
                    s.state = .ValueBegin;
                },
                '{' => {
                    try s.cs.open(.InlineTable);
                    token.* = Token.InlineTableBegin;
                    s.state = .IdentifierBegin;
                },
                '\n' => {
                    if (s.cs.state != .Array) return error.UnexpectedEndLine;
                },
                13 => if (!s.curSlice("\r\n")) return error.InvalidCharacter,
                ']' => switch (s.cs.state) {
                    .Array => {
                        try s.cs.close(.Array);
                        token.* = Token.ArrayEnd;
                        s.state = .ValueEnd;
                    },
                    else => return error.InvalidCharacter,
                },
                else => {
                    // std.log.warn("{}", .{c});
                    return error.InvalidCharacter;
                },
            },
            .ValueEnd => {
                s.dtm_offset = 0;
                switch(c) {
                    '#' => switch (s.cs.state) {
                        .None => {
                            s.state = .Comment;
                        },
                        .Array => {
                            s.prev_state = .ValueEnd;
                            s.state = .Comment;
                        },
                        else => return error.InvalidCharacter,
                    },
                    ' ', '\t' => {},
                    ',' => switch (s.cs.state) {
                        .Array => {
                            token.* = Token.Comma;
                            s.state = .ValueBegin;
                        },
                        .InlineTable => {
                            token.* = Token.Comma;
                            s.need_id = true;
                            s.state = .IdentifierBegin;
                        },
                        else => return error.InvalidCharacter,
                    },
                    ']' => switch (s.cs.state) {
                        .Array => {
                            try s.cs.close(.Array);
                            token.* = Token.ArrayEnd;
                        },
                        else => return error.InvalidCharacter,
                    },
                    '}' => switch (s.cs.state) {
                        .InlineTable => {
                            try s.cs.close(.InlineTable);
                            token.* = Token.InlineTableEnd;
                        },
                        else => return error.InvalidCharacter,
                    },
                    13 => if (!s.curSlice("\r\n")) return error.InvalidCharacter,
                    '\n' => switch (s.cs.state) {
                        .Array => {},
                        .None => {
                            token.* = Token.EndLine;
                            s.state = .NewLine;
                        },
                        else => return error.UnexpectedEndLine,
                    },
                    else => {
                        // std.log.warn("{c}", .{c});
                        return error.InvalidCharacter;
                    }
                }
            },
            .StringLiteral => {
                switch (c) {
                    '\'' => switch (s.multiline) {
                        true => {
                            if (s.curSlice("''''''")) return error.InvalidStringClose;
                            if (s.curSlice("'''") and (!s.curSlice("''''"))) s.string_end = true;
                        },
                        false => s.string_end = true,
                    },
                    '\n', 13 => {
                        if (!s.multiline) return error.UnexpectedEndLine;
                    },
                    0, 8, 16, 31, 127, 192...195, 245...255 => return error.InvalidCharacter,
                    else => {},
                }
                
                if (s.string_end) {
                    if (s.input[s.start] == '\n') s.start += 1; // if start is new line trim then.
                    var string = .{
                        .slice = s.input[s.start..s.i],
                        .literal = true,
                        .multiline = s.multiline,
                        .escapes = s.escapes,
                    };
                    switch (s.id_string) {
                        true => {
                            token.* = Token { .Identifier = string };
                            s.state = .IdentifierEnd;
                        },
                        false => {
                            token.* = Token { .String = string };
                            s.state = .ValueEnd;
                        }
                    }
                    if (s.multiline) s.i += 2;
                    s.string_end = false;
                }
            },
            .StringBasic => {
                switch (c) {
                    '"' => switch (s.multiline) {
                        true => {
                            if (s.curSlice("\"\"\"\"\"\"")) return error.InvalidStringClose;
                            if (s.curSlice("\"\"\"") and (!s.curSlice("\"\"\"\""))) s.string_end = true;
                        },
                        false => s.string_end = true,
                    },
                    '\n', 13 => {
                        if (!s.multiline) return error.UnexpectedEndLine;
                    },
                    '\\' => {
                        s.escapes = true;
                        s.state = .StringEscape;
                    },
                    0, 8, 16, 31, 127, 192...195, 245...255 => return error.InvalidCharacter,
                    else => {},
                }
                if (s.string_end) {
                    if (s.input[s.start] == '\n') s.start += 1; // if start is new line trim then.
                    var string = .{
                        .slice = s.input[s.start..s.i],
                        .literal = false,
                        .multiline = s.multiline,
                        .escapes = s.escapes,
                    };
                    switch (s.id_string) {
                        true => {
                            token.* = .{ .Identifier = string };
                            s.state = .IdentifierEnd;
                        },
                        false => {
                            token.* = .{ .String = string };
                            s.state = .ValueEnd;
                        }
                    }
                    if (s.multiline) s.i += 2;
                    s.string_end = false;
                }
            },
            .StringEscape => switch (c) {
                'u' => {
                    if (s.input.len < s.i + 4) return error.InvalidCharacter;
                    if (!isValidUtf(s.input[s.i + 1.. s.i + 5])) return error.InvalidCharacter;
                    s.i += 4;
                    s.state = .StringBasic;
                },
                'U' => {
                    if (s.input.len < s.i + 8) return error.InvalidCharacter;
                    if (!isValidUtf(s.input[s.i + 1.. s.i + 9])) return error.InvalidCharacter;
                    s.i += 8;
                    s.state = .StringBasic;
                },
                'b', 'f', 'n', 'r', 't', '\"', '\\', '\n' => {
                    s.state = .StringBasic;
                },
                ' ' => {
                    if (!s.multiline) return error.InvalidCharacter;
                    s.state = .ToEndLine;
                },
                else => s.state = .StringBasic,
            },
            .ToEndLine => switch (c) {
                ' ', '\t' => {},
                '\n' => s.state = .StringBasic,
                13 => if (!s.curSlice("\r\n")) return error.InvalidCharacter,
                else => return error.InvalidCharacter,
            },
            
            .Zero => switch (c) {
                'e', 'E' => s.state = .FloatExponent,
                '0'...'9' => s.state = .DateTime,
                'b' => {
                    s.start += 2;
                    s.state = .Binary;
                },
                'o' => {
                    s.start += 2;
                    s.state = .Octal;
                },
                'x' => {
                    s.start += 2;
                    s.state = .Hexa;
                },
                '.' => s.state = .FloatFract,
                else => {
                    token.* = .{ .Integer = .{ .slice = s.input[s.start..s.i], .radix = 10}};
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .SignZero => switch (c) {
                '.' => {
                    s.state = .FloatFract;
                },
                'e', 'E' => s.state = .FloatExponent,
                else => {
                    token.* = .{ .Integer = .{ .slice = s.input[s.start..s.i], .radix = 10}};
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .Sign => switch (c) {
                '0' => {
                    s.state = .SignZero;
                },
                '1'...'9' => {
                    s.state = .Number;
                },
                'i' => switch (s.curSlice("inf")) {
                    true => {
                        token.* = .{ .Float = s.input[s.start..s.i + 3] };
                        s.i += 2;
                        s.state = .ValueEnd;
                    },
                    false => return error.InvalidCharacter,
                },
                'n' => switch (s.curSlice("nan")) {
                    true => {
                        token.* = .{ .Float = s.input[s.start + 1..s.i + 3] };
                        s.i += 2;
                        s.state = .ValueEnd;
                    },
                    false => return error.InvalidCharacter,
                },
                else => return error.InvalidCharacter,
            },
            .NumberOrDateTime => switch (c) {
                '_' => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                    s.state = .Number;
                },
                '0'...'9' => {
                    if (s.i - s.start == 4) s.state = .Number;
                },
                '.' => {
                    s.state = .FloatFract;
                },
                'e', 'E' => {
                    s.state = .FloatExponent;
                },
                '-' => {
                    if (s.i - s.start != 4) return error.InvalidCharacter;
                    s.state = .DateTime;
                },
                ':' => {
                    if (s.i - s.start != 2) return error.InvalidCharacter;
                    s.state = .LocalTime;
                },
                else => {
                    token.* = .{ .Integer = .{ .slice = s.input[s.start..s.i], .radix = 10}};
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .Number => switch (c) {
                '_' => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                },
                '0'...'9' => {},
                '.' => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                    s.state = .FloatFract;
                },
                'e', 'E' => s.state = .FloatExponent,
                else => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                    token.* = .{ .Integer = .{ .slice = s.input[s.start..s.i], .radix = 10}};
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .FloatFract => switch (c) {
                '0'...'9' => s.state = .FloatFractional,
                else => return error.InvalidCharacter,
            },
            .FloatFractional => switch (c) {
                '_' => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                },
                '0'...'9' => {},
                'e', 'E' => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                    s.state = .FloatExponent;
                },
                else => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                    token.* = .{ .Float = s.input[s.start..s.i] };
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .FloatExponent => switch (c) {
                '0'...'9' => s.state = .Float,
                '+', '-' => s.state = .FloatExponentSign,
                else => return error.InvalidCharacter,
            },
            .FloatExponentSign => switch (c) {
                '0'...'9' => s.state = .Float,
                else => return error.InvalidCharacter,
            },
            .Float => switch (c) {
                '_' => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                },
                '0'...'9' => {},
                else => {
                    if (!isNumber(s.input[s.i - 1])) return error.InvalidCharacter;
                    token.* = .{ .Float = s.input[s.start..s.i] };
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .Binary => switch (c) {
                '0', '1', '_' => {},
                else => {
                    token.* = .{ .Integer = .{ .slice = s.input[s.start..s.i], .radix = 2}};
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .Octal => switch (c) {
                '0'...'7', '_' => {},
                else => {
                    token.* = .{ .Integer = .{ .slice = s.input[s.start..s.i], .radix = 8}};
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .Hexa => switch (c) {
                '0'...'9', 'a'...'f', 'A'...'F', '_' => {},
                else => {
                    token.* = .{ .Integer = .{ .slice = s.input[s.start..s.i], .radix = 16}};
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .DateTime => switch (s.i - s.start - s.dtm_offset) {
                0, 1, 3, 5, 6, 8, 9, 12, 14, 15, 17, 18, 20, 21, 23 => {
                    switch (c) {
                        '0'...'9' => {},
                        '#' => s.state = .Comment,
                        else => return error.InvalidCharacter,
                    }
                },
                11 => switch(c) {
                    '0'...'9' => {},
                    else => {
                        if (s.input[s.i - 1] == ' ') {
                            token.* = .{ .LocalDate = s.input[s.start..s.i] };
                            if (s.pos) |p| p.col -= 1;
                            s.i -= 1;
                            s.state = .ValueEnd;
                        } else return error.InvalidCharacter;
                    }
                },
                2 => switch (c) {
                    '0'...'9' => {},
                    ':' => s.state = .LocalTime,
                    else => return error.InvalidCharacter,
                },
                4, 7 => {
                    if (c != '-') return error.InvalidCharacter;
                },
                10 => switch (c) {
                    'T', 't', ' ' => {},
                    else => {
                        token.* = .{ .LocalDate = s.input[s.start..s.i] };
                        if (s.pos) |p| p.col -= 1;
                        s.i -= 1;
                        s.state = .ValueEnd;
                    }
                },
                13, 16, 22 => {
                    if (c != ':') return error.InvalidCharacter;
                },
                19 => switch (c) {
                    '+', '-' => {},
                    '.' => {
                        s.dtm_offset = 0;
                        s.state = .DateTimeMiliseconds;
                    },
                    'Z', 'z' => {
                        token.* = .{ .DateTime = s.input[s.start..s.i + 1] };
                        s.state = .ValueEnd;
                    },
                    else => {
                        token.* = .{ .LocalDateTime = s.input[s.start..s.i] };
                        if (s.pos) |p| p.col -= 1;
                        s.i -= 1;
                        s.state = .ValueEnd;
                    }
                },
                24 => switch (c) {
                    '0'...'9' => {
                        token.* = .{ .DateTime = s.input[s.start..s.i + 1] };
                        s.state = .ValueEnd;
                    },
                    else => return error.InvalidCharacter,
                },
                else => unreachable,
            },
            .DateTimeMiliseconds => switch (c) {
                '0'...'9' => s.dtm_offset += 1,
                '+', '-' => {
                    s.dtm_offset += 1;
                    s.state = .DateTime;
                },
                'Z', 'z' => {
                    token.* = .{ .DateTime = s.input[s.start..s.i + 1] };
                    s.state = .ValueEnd;
                },
                else => {
                    token.* = .{ .LocalDateTime = s.input[s.start..s.i] };
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
            },
            .LocalTime => switch (s.i - s.start) {
                0, 1, 3, 4, 6, 7 => switch (c) {
                    '0'...'9' => {},
                    else => return error.InvalidCharacter,
                },
                2, 5 => {
                    if (c != ':') return error.InvalidCharacter;
                },
                8 => switch (c) {
                    '.' => {},
                    else => {
                        token.* = .{ .LocalTime = s.input[s.start..s.i] };
                        if (s.pos) |p| p.col -= 1;
                        s.i -= 1;
                        s.state = .ValueEnd;
                    },
                },
                9...14 => switch (c) {
                    '0'...'9' => {},
                    else => {
                        token.* = .{ .LocalTime = s.input[s.start..s.i] };
                        if (s.pos) |p| p.col -= 1;
                        s.i -= 1;
                        s.state = .ValueEnd;
                    },
                },
                15 => {
                    token.* = .{ .LocalTime = s.input[s.start..s.i] };
                    if (s.pos) |p| p.col -= 1;
                    s.i -= 1;
                    s.state = .ValueEnd;
                },
                else => unreachable,
            }
        }
    }
    
};

const TblType = enum(u3) { None, Implicit, Explicit, ImplicitKey, ExplicitKey, Inline };

pub const Table = struct {
    tbl_type: TblType,
    keys: std.StringArrayHashMap(Value),
    
    pub fn init(a: std.mem.Allocator) Table {
        return Table {
            .keys = std.StringArrayHashMap(Value).init(a),
            .tbl_type = .None,
        };
    }
};

fn putTable(root: *Table, key: []const u8, tbl_type: TblType) Error!*Table {
    if (root.keys.getEntry(key)) |t| switch(t.value_ptr.*) {
        .Table => |found_tbl| {
            const found_tbl_type = found_tbl.tbl_type;
            if (found_tbl_type == .Inline) return error.KeyAlreadyExist;
            switch(tbl_type) {
                .Explicit => switch(found_tbl_type) {
                    .Explicit, .ExplicitKey => return error.KeyAlreadyExist,
                    else => t.value_ptr.Table.tbl_type = .Explicit,
                },
                .ImplicitKey, .ExplicitKey => if (found_tbl_type == .Explicit) return error.KeyAlreadyExist,
                else => {},
            }
        },
        .TableArray => {
            if (tbl_type == .Explicit) return error.KeyAlreadyExist;
            return &t.value_ptr.TableArray.items[t.value_ptr.TableArray.items.len - 1];
        },
        else => return error.KeyAlreadyExist,
    };
    var r = try root.keys.getOrPutValue(key, Value{.Table = Table.init(root.keys.allocator)});
    r.value_ptr.*.Table.tbl_type = tbl_type;
    return &r.value_ptr.Table;
}

fn putTableArray(root: *Table, key: []const u8) Error!*Table {
    if (root.keys.getEntry(key)) |t| {
        switch(t.value_ptr.*) {
            .TableArray => {},
            else => return error.KeyAlreadyExist,
        }
    }
    var ta = try root.keys.getOrPutValue(key, Value{.TableArray = TableArray.init(root.keys.allocator)});
    try ta.value_ptr.*.TableArray.append(Table.init(root.keys.allocator));
    return &ta.value_ptr.*.TableArray.items[ta.value_ptr.*.TableArray.items.len - 1];
}

pub const TableArray = std.ArrayList(Table);
pub const Array = std.ArrayList(Value);

pub const Value = union(enum) {
    // None,
    String: []const u8,
    Integer: i64,
    Float: f64,
    Boolean: bool,
    Array: Array,
    Table: Table,
    TableArray: TableArray,
    DateTime: []const u8,
    LocalDateTime: []const u8,
    LocalDate: []const u8,
    LocalTime: []const u8,
};

pub const ValueTree = struct {
    arena: std.heap.ArenaAllocator,
    root: Table,

    pub fn deinit(self: *ValueTree) void {
        self.arena.deinit();
    }
};

pub fn parse(allocator: std.mem.Allocator, input: []const u8, pos: ?*Position) Error!ValueTree {
    var state: enum(u8) {
        None,
        NewLine,
        Table,
        TableArray,
        IdentifierBegin,
        Identifier,
        Value,
    } = .NewLine;
    
    var vt: ValueTree = undefined;
    vt.arena = std.heap.ArenaAllocator.init(allocator);
    vt.root = Table.init(vt.arena.allocator());
    const a = vt.arena.allocator();
    
    var table = &vt.root;
    var key_table = &vt.root;
    errdefer {
        vt.arena.deinit();
    }
        
    var key: []const u8 = undefined;
    var key_prev: []const u8 = undefined;
    
    var ts = Tokenizer.init(input, pos);
    
    while (try ts.next()) |t| {
        switch(state) {
            .NewLine => switch(t){
                .Identifier => {
                    key_table = table;
                    key = if (t.Identifier.escapes) try parseString(a, t.Identifier) else t.Identifier.slice;
                    key_prev = "";
                    state = .Identifier;
                },
                .TableBegin => {
                    table = &vt.root;
                    state = .Table;
                },
                .TableArrayBegin => {
                    table = &vt.root;
                    state = .TableArray;
                },
                else => {},
            },
            .Identifier => switch(t){
                .Dot => {
                    if (key_prev.len > 0) key_table = try putTable(key_table, key_prev, .ImplicitKey);
                    key_prev = key;
                },
                .Identifier => {
                    key = if (t.Identifier.escapes) try parseString(a, t.Identifier) else t.Identifier.slice;
                },
                .Equals => {
                    if (key_prev.len > 0) key_table = try putTable(key_table, key_prev, .ExplicitKey);
                    state = .Value;
                },
                else => {},
            },
            .Value => {
                key_table.keys.putNoClobber(key, try parseValue(a, &ts, t)) catch return error.KeyAlreadyExist;
                state = .NewLine;
            },
            .Table => switch(t){
                .Identifier => {
                    key = if (t.Identifier.escapes) try parseString(a, t.Identifier) else t.Identifier.slice;
                },
                .Dot => {
                    table = try putTable(table, key, .Implicit);
                },
                .TableEnd => {
                    table = try putTable(table, key, .Explicit);
                    state = .NewLine;
                },
                else => {},
            },
            .TableArray => switch(t) {
                .Identifier => {
                    key = t.Identifier.slice;
                },
                .Dot => {
                    table = try putTable(table, key, .Implicit);
                },
                .TableArrayEnd => {
                    table = try putTableArray(table, key);
                    state = .NewLine;
                },
                else => return error.UnexpectedToken,
            },
            else => {},
        }
    }
    
    return vt;
}

fn parseValue(a: std.mem.Allocator, ts: *Tokenizer, t: Token) Error!Value {
    switch(t) {
        .Integer => |v| {
            return Value{.Integer = try parseInt(i64, v.slice, v.radix)};
        },
        .Float => |v| {
            return Value{.Float = try parseFloat(f64, v)};
        },
        .Boolean => |v| {
            return Value{.Boolean = v};
        },
        .String => |v| {
            return Value{ .String = if((!v.escapes and !v.multiline) or v.literal) v.slice else try parseString(a, v)};
        },
        .DateTime => |v| {
            if (!validDateTime(t)) return error.InvalidValue;
            return Value{ .DateTime = v};
        },
        .LocalDateTime => |v| {
            if (!validDateTime(t)) return error.InvalidValue;
            return Value{ .LocalDateTime = v};
        },
        .LocalDate => |v| {
            if (!validDateTime(t)) return error.InvalidValue;
            return Value{ .LocalDate = v};
        },
        .LocalTime => |v| {
            if (!validDateTime(t)) return error.InvalidValue;
            return Value{ .LocalTime = v};
        },
        .ArrayBegin => {
            return try parseArray(a, ts);
        },
        .InlineTableBegin => {
            return try parseInlineTable(a, ts);
        },
        else => return error.UnexpectedToken,
    }
}

fn parseArray(a: std.mem.Allocator, ts: *Tokenizer) Error!Value {
    var array = Array.init(a);
    
    while (try ts.next()) |t| {
        switch(t) {
            .Comma => {},
            .ArrayEnd => return Value{ .Array = array},
            else => try array.append(try parseValue(a, ts, t)),
        }
    }
    return error.UnexpectedToken;
}

fn parseInlineTable(a: std.mem.Allocator, ts: *Tokenizer) Error!Value {
    var state: enum(u8) {
        NewKey,
        Value,
    } = .NewKey;
    
    var tbl = Table.init(a);
    var keyTable = &tbl;
    var key: []const u8 = undefined;
    
    while(try ts.next()) |t| {
        switch(state) {
            .NewKey => switch(t) {
                .Comma => {
                },
                .InlineTableEnd => {
                    tbl.tbl_type = .Inline;
                    return Value{ .Table = tbl};
                },
                .Identifier => {
                    key = t.Identifier.slice;
                },
                .Dot => {
                    keyTable = try putTable(keyTable, key, .ImplicitKey);
                },
                .Equals => {
                    state = .Value;
                },
                else => return error.UnexpectedToken,
            },
            .Value => {
                keyTable.keys.putNoClobber(key, try parseValue(a, ts, t)) catch return error.KeyAlreadyExist;
                keyTable = &tbl;
                state = .NewKey;
            },
        }
    }
    return error.UnexpectedToken;
}

fn validDateTime(dt: Token) bool {
    switch (dt) {
        .DateTime => |v| {
            // const year = try parseInt(u16, v[0..4], 10);
            const month = parseInt(u16, v[5..7], 10) catch return false;
            const day = parseInt(u16, v[8..10], 10) catch return false;
            const hour = parseInt(u16, v[11..13], 10) catch return false;
            const minute = parseInt(u16, v[14..16], 10) catch return false;
            const second = parseInt(u16, v[17..19], 10) catch return false;
            
            if (hour > 23 or minute > 59 or second > 59 or month < 1 or month > 12 or day < 1 or day > 31) return false;
        },
        .LocalDateTime => |v| {
            // const year = try parseInt(u16, v[0..4], 10);
            const month = parseInt(u16, v[5..7], 10) catch return false;
            const day = parseInt(u16, v[8..10], 10) catch return false;
            const hour = parseInt(u16, v[11..13], 10) catch return false;
            const minute = parseInt(u16, v[14..16], 10) catch return false;
            const second = parseInt(u16, v[17..19], 10) catch return false;
            
            if (hour > 23 or minute > 59 or second > 59 or month < 1 or month > 12 or day < 1 or day > 31) return false;
        },
        .LocalDate => |v| {
            // const year = try parseInt(u16, v[0..4], 10);
            const month = parseInt(u16, v[5..7], 10) catch return false;
            const day = parseInt(u16, v[8..10], 10) catch return false;
            if (month < 1 or month > 12 or day < 1 or day > 31) return false;
        },
        .LocalTime => |v| {
            const hour = parseInt(u16, v[0..2], 10) catch return false;
            const minute = parseInt(u16, v[3..5], 10) catch return false;
            const second = parseInt(u16, v[6..8], 10) catch return false;

            if (hour > 23 or minute > 59 or second > 59) return false;
        },
        else => unreachable,
    }
    return true;
}

fn parseString(allocator: std.mem.Allocator, s: String) Error![]const u8 {
    var out: []u8 = try allocator.alloc(u8, @sizeOf(u8) * s.slice.len);
    defer allocator.free(out);

    var inx: usize = 0;
    var ucodeStart: usize = 0;
    var ucodeSize: u8 = 0;

    var state: enum(u8) {
        Normal,
        Escape,
        Ignore,
        Ucode,
    } = .Normal;

    for(s.slice, 0..) |c, i| {
        switch(state) {
            .Escape => {
                state = .Normal;
                switch(c) {
                    'b' => { out[inx] = 8; inx += 1; },
                    't' => { out[inx] = '\t'; inx += 1; },
                    'n' => { out[inx] = '\n'; inx += 1; },
                    'f' => { out[inx] = 12; inx += 1; },
                    'r' => { out[inx] = '\r'; inx += 1; },
                    '"' => { out[inx] = c; inx += 1; },
                    '\\' => { out[inx] = c; inx += 1; },
                    0x08, 0x09, 0x0A, 0x0D, 0x20 => state = .Ignore,
                    'u' => { state = .Ucode; ucodeSize = 4; ucodeStart = i + 1; },
                    'U' => { state = .Ucode; ucodeSize = 8; ucodeStart = i + 1; },
                    else => return error.InvalidCharacter,
                }
            },
            .Ucode => {
                if (i == ucodeStart + ucodeSize - 1) {
                    const cp = std.fmt.parseInt(u21, s.slice[ucodeStart..i + 1], 16) catch return error.InvalidCharacter;
                    var b: [8]u8 = undefined;
                    const len = std.unicode.utf8Encode(cp, b[0..]) catch return error.InvalidCharacter;
                    for (b[0..len], 0..) |u, ix| {
                        out[inx + ix] = u;
                    }
                    inx += len;
                    
                    ucodeSize = 0;
                    state = .Normal;
                }
            },
            .Normal => switch(c) {
                '\\' => state = .Escape,
                else => { out[inx] = c; inx += 1; },
            },
            .Ignore => switch(c) {
                0x08, 0x09, 0x0A, 0x0D, 0x20 => {},
                '\\' => state = .Escape,
                else => { out[inx] = c; inx += 1; state = .Normal; },
            },
        }
    }
    if (ucodeSize != 0) return error.InvalidCharacter;
    return try allocator.dupe(u8, out[0..inx]);
}

pub fn table2json(table: Table, output: anytype, type_value: bool) Error!void {
    var first: bool = true;
    var iter = table.keys.iterator();
    try output.writeByte('{');
    while (iter.next()) |item| {
        if (!first) try output.writeByte(',');
        first = false;
        try output.writeByte('\"');
        try encodeString(item.key_ptr.*, output);
        try output.writeAll("\":");
        try value2json(item.value_ptr.*, output, type_value);
    }
    try output.writeByte('}');
}

fn value2json(value: Value, output: anytype, type_value: bool) Error!void {
    switch (value) {
        .Integer => |v| {
            if (type_value) try std.fmt.format(output, "{{\"type\":\"integer\",\"value\":\"{}\"}}", .{v})
            else try std.fmt.format(output, "{}", .{v});
        },
        .Float => |v| {
            if (type_value) try std.fmt.format(output, "{{\"type\":\"float\",\"value\":\"{d}\"}}", .{v})
            else try std.fmt.format(output, "{d}", .{v});
        },
        .Boolean => |v| {
            if (type_value) try std.fmt.format(output, "{{\"type\":\"bool\",\"value\":\"{}\"}}", .{v})
            else try std.fmt.format(output, "{}", .{v});
        },
        .String => |v| {
            if (type_value) {
                try output.writeAll("{\"type\": \"string\",\"value\":\"");
                try encodeString(v, output);
                try output.writeAll("\"}");
            } else {
                try output.writeAll("\"");
                try encodeString(v, output);
                try output.writeAll("\"");
            }
        },
        .DateTime => |v| { 
            if (type_value) try std.fmt.format(output, "{{\"type\":\"datetime\",\"value\":\"{s}\"}}", .{v})
            else try std.fmt.format(output, "\"{s}\"", .{v});
        },
        .LocalDateTime => |v| {
            if (type_value) try std.fmt.format(output, "{{\"type\":\"datetime-local\",\"value\":\"{s}\"}}", .{v})
            else try std.fmt.format(output, "\"{s}\"", .{v});
        },
        .LocalDate => |v| {
            if (type_value) try std.fmt.format(output, "{{\"type\":\"date-local\",\"value\":\"{s}\"}}", .{v})
            else try std.fmt.format(output, "\"{s}\"", .{v});
        },
        .LocalTime => |v| {
            if (type_value) try std.fmt.format(output, "{{\"type\":\"time-local\", \"value\":\"{s}\"}}", .{v})
            else try std.fmt.format(output, "\"{s}\"", .{v});
        },
        
        .Array => |v| {
            try output.writeByte('[');
            for (v.items, 0..) |item, i| {
                if (i > 0) try output.writeByte(',');
                try value2json(item, output, type_value);
            }
            try output.writeByte(']');
        },
        .Table => |v| {
            try table2json(v, output, type_value);
        },
        .TableArray => |v| {
            try output.writeByte('[');
            for (v.items, 0..) |item, i| {
                if (i > 0) try output.writeByte(',');
                try table2json(item, output, type_value);
            }
            try output.writeByte(']');
        },
        // else => unreachable,
    }
}

fn encodeString(str: []const u8, output: anytype) !void {
    for (str) |c| {
        switch (c) {
            '"' => try output.writeAll("\\\""),
            '\\' => try output.writeAll("\\\\"),
            '\n' => try output.writeAll("\\n"),
            '\r' => try output.writeAll("\\r"),
            '\t' => try output.writeAll("\\t"),
            8 => try output.writeAll("\\u0008"),
            12 => try output.writeAll("\\u000c"),
            31 => try output.writeAll("\\u001f"),
            else => try output.writeByte(c),
        }
    }
}

pub const ParseOptions = struct {
    /// If false, finding an unknown field returns an error.
    ignore_unknown_fields: bool = false,
    type_value: bool = false,
};


fn table2struct(a: std.mem.Allocator, table: Table, comptime T: type, po: ParseOptions) Error!T {
    var json_str = std.ArrayList(u8).init(a);
    defer json_str.deinit();

    try table2json(table, json_str.writer(), po.type_value);

    var stream = std.json.TokenStream.init(json_str.items);
    var result = std.json.parse(T, &stream, .{
        .allocator = a,
        .ignore_unknown_fields = po.ignore_unknown_fields,
    }) catch return error.InvalidValue;
    return result;
}

test "table2struct" {
    const a = std.testing.allocator;
    const input =
        \\
        \\bool1 = true
        \\bool2 = false
        \\
    ;

    const Data = struct {
        bool1: bool,
        bool2: bool,
    };

    var vt = try parse(a, input, null);
    defer vt.deinit();

    var data = try table2struct(a, vt.root, Data, .{});
    defer std.json.parseFree(Data, data, .{ .allocator = a });
    assert(data.bool1);
    assert(!data.bool2);
}

test "json value" {
    const a = std.testing.allocator;
    const input =
        \\
        \\bool1 = true
        \\bool2 = false
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    var jsonstr = std.ArrayList(u8).init(a);
    defer jsonstr.deinit();

    try table2json(vt.root, jsonstr.writer(), false);

    const Data = struct {
        bool1: bool,
        bool2: bool,
    };

    var stream = std.json.TokenStream.init(jsonstr.items);
    var data = try std.json.parse(Data, &stream, .{ .allocator = a });

    assert(data.bool1);
    assert(!data.bool2);
    
}

test "String" {
    const a = std.testing.allocator;
    const input =
        \\
        \\# The following strings are byte-for-byte equivalent:
        \\str1 = "The quick brown fox jumps over the lazy dog."
        \\
        \\str2 = """
        \\The quick brown \
        \\
        \\
        \\  fox jumps over \
        \\    the lazy dog."""

        \\str3 = """\
        \\       The quick brown \
        \\       fox jumps over \
        \\       the lazy dog.\
        \\       """
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    const str = "The quick brown fox jumps over the lazy dog.";
    assert(std.mem.eql(u8, str, vt.root.keys.get("str1").?.String));
    assert(std.mem.eql(u8, str, vt.root.keys.get("str2").?.String));
    assert(std.mem.eql(u8, str, vt.root.keys.get("str3").?.String));
}

test "Integer" {
    const a = std.testing.allocator;
    const input =
        \\
        \\int1 = +99
        \\int2 = 42
        \\int3 = 0
        \\int4 = -17
        \\int5 = 1_000
        \\int6 = 5_349_221
        \\int7 = 53_49_221  # Indian number system grouping
        \\int8 = 1_2_3_4_5  # VALID but discouraged
        \\
        \\# hexadecimal with prefix `0x`
        \\hex1 = 0xDEADBEEF
        \\hex2 = 0xdeadbeef
        \\hex3 = 0xdead_beef
        \\
        \\# octal with prefix `0o`
        \\oct1 = 0o01234567
        \\oct2 = 0o755 # useful for Unix file permissions
        \\
        \\# binary with prefix `0b`
        \\bin1 = 0b11010110
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    assert(vt.root.keys.get("int1").?.Integer == 99);
    assert(vt.root.keys.get("int2").?.Integer == 42);
    assert(vt.root.keys.get("int3").?.Integer == 0);
    assert(vt.root.keys.get("int4").?.Integer == -17);
    assert(vt.root.keys.get("int5").?.Integer == 1000);
    assert(vt.root.keys.get("int6").?.Integer == 5349221);
    assert(vt.root.keys.get("int7").?.Integer == 5349221);
    assert(vt.root.keys.get("int8").?.Integer == 12345);
    
    assert(vt.root.keys.get("hex1").?.Integer == 3735928559);
    assert(vt.root.keys.get("hex2").?.Integer == 3735928559);
    assert(vt.root.keys.get("hex3").?.Integer == 3735928559);
    
    assert(vt.root.keys.get("oct1").?.Integer == 342391);
    assert(vt.root.keys.get("oct2").?.Integer == 493);
    
    assert(vt.root.keys.get("bin1").?.Integer == 214);
    
}

test "Float" {
    const a = std.testing.allocator;
    const input =
        \\
        \\# fractional
        \\flt1 = +1.0
        \\flt2 = 3.1415
        \\flt3 = -0.01
        \\
        \\# exponent
        \\flt4 = 5e+22
        \\flt5 = 1e06
        \\flt6 = -2E-2
        \\
        \\# both
        \\flt7 = 6.626e-34
        \\
        \\flt8 = 224_617.445_991_228
        \\
        \\# infinity
        \\sf1 = inf  # positive infinity
        \\sf2 = +inf # positive infinity
        \\sf3 = -inf # negative infinity
        \\
        \\# not a number
        \\sf4 = nan  # actual sNaN/qNaN encoding is implementation-specific
        \\sf5 = +nan # same as `nan`
        \\sf6 = -nan # valid, actual encoding is implementation-specific
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    assert(vt.root.keys.get("flt1").?.Float == 1.0);
    assert(vt.root.keys.get("flt2").?.Float == 3.1415);
    assert(vt.root.keys.get("flt3").?.Float == -0.01);
    assert(vt.root.keys.get("flt4").?.Float == 5e+22);
    assert(vt.root.keys.get("flt5").?.Float == 1e06);
    assert(vt.root.keys.get("flt6").?.Float == -2E-2);
    assert(vt.root.keys.get("flt7").?.Float == 6.626e-34);
    assert(vt.root.keys.get("flt8").?.Float == 224617.445991228);

    assert(std.math.isPositiveInf(vt.root.keys.get("sf1").?.Float));
    assert(std.math.isPositiveInf(vt.root.keys.get("sf2").?.Float));
    assert(std.math.isNegativeInf(vt.root.keys.get("sf3").?.Float));
    
    assert(std.math.isNan(vt.root.keys.get("sf4").?.Float));
    assert(std.math.isNan(vt.root.keys.get("sf5").?.Float));
    assert(std.math.isNan(vt.root.keys.get("sf6").?.Float));
    
}

test "Boolean" {
    const a = std.testing.allocator;
    const input =
        \\
        \\bool1 = true
        \\bool2 = false
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    assert(vt.root.keys.get("bool1").?.Boolean);
    assert(!vt.root.keys.get("bool2").?.Boolean);
    
}

test "Array" {
    const a = std.testing.allocator;
    const input =
        \\
        \\integers = [ 1, 2, 3 ]
        \\colors = [ "red", "yellow", "green" ]
        \\nested_arrays_of_ints = [ [ 1, 2 ], [3, 4, 5] ]
        \\nested_mixed_array = [ [ 1, 2 ], ["a", "b", "c"] ]
        \\
        \\integers2 = [
        \\  1, 2, 3
        \\]
        \\
        \\integers3 = [
        \\  1,
        \\  2, # this is ok
        \\]
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    const integers = vt.root.keys.get("integers").?.Array;
    assert(integers.items[0].Integer == 1);
    assert(integers.items[1].Integer == 2);
    assert(integers.items[2].Integer == 3);

    const colors = vt.root.keys.get("colors").?.Array;
    assert(std.mem.eql(u8, "red", colors.items[0].String));
    assert(std.mem.eql(u8, "yellow", colors.items[1].String));
    assert(std.mem.eql(u8, "green", colors.items[2].String));

    const nai = vt.root.keys.get("nested_arrays_of_ints").?.Array;
    assert(nai.items[0].Array.items[0].Integer == 1);
    assert(nai.items[0].Array.items[1].Integer == 2);
    assert(nai.items[1].Array.items[0].Integer == 3);
    assert(nai.items[1].Array.items[1].Integer == 4);
    assert(nai.items[1].Array.items[2].Integer == 5);
    
    const nma = vt.root.keys.get("nested_mixed_array").?.Array;
    const array_n = nma.items[0].Array;
    assert(array_n.items[0].Integer == 1);
    assert(array_n.items[1].Integer == 2);
    const array_s = nma.items[1].Array;
    assert(std.mem.eql(u8, "a", array_s.items[0].String));
    assert(std.mem.eql(u8, "b", array_s.items[1].String));
    assert(std.mem.eql(u8, "c", array_s.items[2].String));
    
    const integers2 = vt.root.keys.get("integers2").?.Array;
    assert(integers2.items[0].Integer == 1);
    assert(integers2.items[1].Integer == 2);
    assert(integers2.items[2].Integer == 3);
    
    const integers3 = vt.root.keys.get("integers3").?.Array;
    assert(integers3.items[0].Integer == 1);
    assert(integers3.items[1].Integer == 2);
    
}

test "key" {
    const a = std.testing.allocator;
    const input =
        \\
        \\a.b.c = 1
        \\a.'b'.d = 2
        \\"a".'b'.e = 3
        \\"a.b".c = 4
        \\'a.b'.d = 5 
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    const ab = vt.root.keys.get("a").?.Table.keys.get("b").?.Table;
    assert(ab.keys.get("c").?.Integer == 1);
    assert(ab.keys.get("d").?.Integer == 2);
    assert(ab.keys.get("e").?.Integer == 3);
    
    const adotb = vt.root.keys.get("a.b").?.Table;
    assert(adotb.keys.get("c").?.Integer == 4);
    assert(adotb.keys.get("d").?.Integer == 5);
    
}

test "table" {
    const a = std.testing.allocator;
    const input =
        \\
        \\a = 1
        \\b = 2
        \\[tbl1]
        \\a = 3
        \\b = 4
        \\[tbl1.tbl2]
        \\a = 5
        \\b = 6 
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    assert(vt.root.keys.get("a").?.Integer == 1);
    assert(vt.root.keys.get("b").?.Integer == 2);

    const tbl1 = vt.root.keys.get("tbl1").?.Table;
    assert(tbl1.keys.get("a").?.Integer == 3);
    assert(tbl1.keys.get("b").?.Integer == 4);
    
    const tbl2 = vt.root.keys.get("tbl1").?.Table.keys.get("tbl2").?.Table;
    assert(tbl2.keys.get("a").?.Integer == 5);
    assert(tbl2.keys.get("b").?.Integer == 6);
    
}

test "inline table" {
    const a = std.testing.allocator;
    const input =
        \\
        \\itbl1 = {a = 1, b = 2, c = true}
        \\itbl2 = {a = 'true', b = "true", c = true}
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    const itbl1 = vt.root.keys.get("itbl1").?.Table;
    assert(itbl1.keys.get("a").?.Integer == 1);
    assert(itbl1.keys.get("b").?.Integer == 2);
    assert(itbl1.keys.get("c").?.Boolean);
    
    const itbl2 = vt.root.keys.get("itbl2").?.Table;
    assert(std.mem.eql(u8, "true", itbl2.keys.get("a").?.String));
    assert(std.mem.eql(u8, "true", itbl2.keys.get("b").?.String));
    assert(itbl2.keys.get("c").?.Boolean);
    
}

test "nested" {
    const a = std.testing.allocator;
    const input =
        \\
        \\a = {b = [1, {c = 2, d = 3}, ['foo', "bar"]]}
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    const atbl = vt.root.keys.get("a").?.Table;
    
    const b = atbl.keys.get("b").?.Array;
    assert(b.items[0].Integer == 1);

    const itbl = b.items[1].Table;
    assert(itbl.keys.get("c").?.Integer == 2);
    assert(itbl.keys.get("d").?.Integer == 3);

    const arr = b.items[2].Array;
    assert(std.mem.eql(u8, "foo", arr.items[0].String));
    assert(std.mem.eql(u8, "bar", arr.items[1].String));
    
}

test "table array" {
    const a = std.testing.allocator;
    const input =
        \\
        \\[[tbl]]
        \\a = 1
        \\[[tbl]]
        \\a = 2
        \\[[tbl]]
        \\a = 3
        \\
    ;
    
    var vt = try parse(a, input, null);
    defer vt.deinit();

    const tbl = vt.root.keys.get("tbl").?.TableArray;
    assert(tbl.items[0].keys.get("a").?.Integer == 1);
    assert(tbl.items[1].keys.get("a").?.Integer == 2);
    assert(tbl.items[2].keys.get("a").?.Integer == 3);
    
}

