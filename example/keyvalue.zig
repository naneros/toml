const std = @import("std");
const toml = @import("toml");

var allocator = std.heap.page_allocator;
const stdout = std.io.getStdOut().writer();

pub fn main()!void {
    const input = 
        \\a = 12345
        \\b = true
        \\c = "Hello"
    ;
    
    var vt = try toml.parse(allocator, input, null);
    defer vt.deinit();

    try stdout.print("a={}\n", .{vt.root.keys.get("a").?.Integer});
    try stdout.print("b={}\n", .{vt.root.keys.get("b").?.Boolean});
    try stdout.print("c={s}\n", .{vt.root.keys.get("c").?.String});
}

